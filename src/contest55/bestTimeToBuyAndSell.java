package contest55;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class bestTimeToBuyAndSell {
    public static void main(String[] args) {
        new bestTimeToBuyAndSell().run();
    }
    public void run(){
        int[] price={1,4,6,2,8,3,10,14};
        int fee=3;
        int max=maxProfit(price,fee);
        System.out.println(max);
    }
    public int maxProfit(int[] prices,int fee){
        HashMap<String, Integer> map=new LinkedHashMap<>();
        for (int i = 0; i <prices.length; i++) {
            for (int j =i+1; j <prices.length; j++) {
                int profit=prices[j]-prices[i]-fee;
                if (profit>0){
                    map.put(String.valueOf(i)+String.valueOf(j),profit);
                }
            }
        }
        int maxprofit=0;
        for (String s:map.keySet()){
            int count=1;
            int profit=0;
            int lastIndex=0;
            for (String l:map.keySet() ) {
                if (count==1){
                    profit=map.get(s);
                    lastIndex=Integer.parseInt(s.substring(1,2));
                }
                if (Integer.parseInt(l.substring(0,1))>lastIndex){
                    profit=profit+map.get(l);
                    lastIndex=Integer.parseInt(l.substring(1,2));
                }
                count++;
            }
            if (profit>maxprofit){
                maxprofit=profit;
            }
        }
        return maxprofit;
    }
}
